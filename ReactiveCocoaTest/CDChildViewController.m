//
//  CDChildViewController.m
//  ReactiveCocoaTest
//
//  Created by Norm Barnard on 9/24/13.
//  Copyright (c) 2013 Clamdango. All rights reserved.
//

#import "CDChildViewController.h"

@interface CDChildViewController ()

@property (weak, nonatomic) IBOutlet UIButton *modeOneButton;
@property (weak, nonatomic) IBOutlet UIButton *modeTwoButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@end

@implementation CDChildViewController

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    return self;
}

- (NSString *)nibName
{
    return @"CDChildView";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)_modeButtonTapped:(UIButton *)sender
{
    ChildMode mode = (sender == [self modeOneButton]) ? ChildModeOne : ChildModeTwo;
    [self setCurrentMode:mode];
}

- (IBAction)_doneButtonTapped:(UIButton *)sender
{
    _currentMode = ChildModeDone;
    [self setCurrentMode:ChildModeDone];
}

@end
