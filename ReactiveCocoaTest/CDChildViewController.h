//
//  CDChildViewController.h
//  ReactiveCocoaTest
//
//  Created by Norm Barnard on 9/24/13.
//  Copyright (c) 2013 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum ChildMode {
    ChildModeNone,
    ChildModeOne,
    ChildModeTwo,
    ChildModeDone
} ChildMode;


@interface CDChildViewController : UIViewController

@property (nonatomic, assign) ChildMode currentMode;

@end
