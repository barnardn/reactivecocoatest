//
//  CDRootViewController.m
//  ReactiveCocoaTest
//
//  Created by Norm Barnard on 9/24/13.
//  Copyright (c) 2013 Clamdango. All rights reserved.
//

#import "CDChildViewController.h"
#import "CDRootViewController.h"

@interface CDRootViewController ()

@end

@implementation CDRootViewController

- (instancetype)init
{
    self = [super init];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    CDChildViewController *vcc = [[CDChildViewController alloc] init];
    CGRect childFrame = CGRectInset([[self view] bounds], 50.0f, 100.0f);
    [[vcc view] setFrame:childFrame];
    [self addChildViewController:vcc];
    [vcc didMoveToParentViewController:self];
    [[self view] addSubview:[vcc view]];
    
    @weakify(self);
    [[vcc rac_signalForSelector:@selector(setCurrentMode:)] subscribeNext:^(RACTuple *tuple) {
        @strongify(self);
        NSLog(@"mode value is: %@",  tuple);
        NSNumber *mode = [tuple first];
        if ([mode integerValue] == ChildModeDone) {
            UIViewController *cvc = [[self childViewControllers] objectAtIndex:0];
            [cvc removeFromParentViewController];
            [[cvc view] removeFromSuperview];
        }        
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
