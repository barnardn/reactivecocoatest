//
//  main.m
//  ReactiveCocoaTest
//
//  Created by Norm Barnard on 9/24/13.
//  Copyright (c) 2013 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CDAppDelegate class]));
    }
}
